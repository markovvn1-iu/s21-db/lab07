# Lab 07

### Task 1

See `src/ex1.sql` and `src/ex1.py`.

![ex1](.gitlab/ex1.png "ex1")

### Task 2

See `src/ex2.sql`

![ex2_1](.gitlab/ex2_1.png "ex2_1")
![ex2_2](.gitlab/ex2_2.png "ex2_2")

### Reproducing

To run use:
```
docker-compose build
docker-compose up
```