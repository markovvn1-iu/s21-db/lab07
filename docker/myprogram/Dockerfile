FROM ubuntu:18.04


# Install basic tools, configure
RUN set -ex \
	&& apt-get -qq update \
	&& apt-get -qq upgrade \
	&& apt-get -qq install locales \
	\
	# Сonfiguring timezone
	&& TZ=Europe/Moscow \
	&& ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
	&& apt-get -qq install tzdata \
	\
	# Russian language
	&& sed -i -e 's/# ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/' /etc/locale.gen \
	&& locale-gen \
	\
	# Clean
	&& apt-get -qq clean \
	&& rm -rf /var/lib/apt/lists/*


# Install python3
RUN set -ex \
	&& apt-get -qq update \
	&& apt-get -qq install python3 python3-pip python3-dev \
	&& update-alternatives --install /usr/bin/python python $(which python3) 1 \
	\
	# Install packages for Python3
	&& python -m pip install --upgrade pip \
	&& python -m pip install setuptools wheel \
	\
	# Clean up
	&& apt-get -qq autoremove \
	&& apt-get -qq clean \
	&& rm -rf /var/lib/apt/lists/* \
	&& (python -m pip cache purge || true) \
	&& rm -rf /root/.cache/*


# Install all stuff for work
RUN set -ex \
	&& apt-get -qq update \
	&& apt-get -qq install libpq-dev postgresql \
	\
	&& python -m pip install psycopg2 faker coloredlogs geopy \
	\
	# Clean up
	&& apt-get -qq clean \
	&& rm -rf /var/lib/apt/lists/* \
	&& (python -m pip cache purge || true) \
	&& rm -rf /root/.cache/*


ENV LANG C.UTF-8
WORKDIR /root/src