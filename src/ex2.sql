CREATE OR REPLACE FUNCTION ex2_function(start_id integer, end_id integer)
RETURNS TABLE(customer_id integer, store_id smallint, first_name VARCHAR(45), last_name VARCHAR(45), email VARCHAR(50),
			 address_id smallint, activebool boolean, create_date date, last_update timestamp without time zone, active integer) AS
$$
BEGIN
	-- notice: if start_id or end_id is negative, error will raised automaticaly --
	IF start_id > 600 or end_id > 600 THEN
		RAISE EXCEPTION 'start_id or end_id grater than 600';
	ELSE
		RETURN QUERY
		SELECT * FROM customer AS cus
		ORDER BY cus.customer_id
		LIMIT end_id-start_id OFFSET start_id-1;
	END IF;
END;
$$
LANGUAGE plpgsql;