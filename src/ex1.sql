
-- Create function --
CREATE OR REPLACE FUNCTION ex1_function()
RETURNS TABLE(address_id integer, address VARCHAR(50)) AS
$$
BEGIN
	RETURN QUERY
	SELECT addr.address_id, addr.address
	FROM address AS addr
	WHERE addr.city_id > 400 AND addr.city_id < 600
	AND addr.address LIKE '%11%';
END;
$$
LANGUAGE plpgsql;


-- Create two columns --
ALTER TABLE address
    ADD COLUMN IF NOT EXISTS longitude double precision;

ALTER TABLE address
    ADD COLUMN IF NOT EXISTS latitude double precision;