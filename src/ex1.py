#!/usr/bin/env python3

import os
import psycopg2
import time
import logging

from geopy.geocoders import Nominatim


logging.basicConfig(format='%(asctime)s [%(name)s] [%(levelname)s] %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def update_lon_lat(con, address_id, lon, lat):
    cur = con.cursor()
    cur.execute("""UPDATE address
        SET longitude=%s, latitude=%s
        WHERE address_id=%s""", (lon, lat, address_id))


def execute_script_file(con, file_name):
    if not os.path.isfile(file_name):
        return

    with open(file_name, "r") as f:
        query = f.read()

    if query.strip():
        cur = con.cursor()
        cur.execute(query)
        con.commit()


def select_ex1_func(con):
    cur = con.cursor()
    cur.callproc("ex1_function")
    return cur.fetchall()


def process(con):
    #execute_script_file(con, "ex1.sql")

    function_res = select_ex1_func(con)

    geolocator = Nominatim(user_agent="ex1")
    for address_id, address in function_res:
        location = geolocator.geocode(address, timeout=20)
        if location is not None:
            update_lon_lat(con, address_id, location.longitude, location.latitude)
        else:
            update_lon_lat(con, address_id, 0, 0)


def main():
    for i in range(20):
        try:
            con = psycopg2.connect(
                database=os.getenv("POSTGRES_DB"),
                user=os.getenv("POSTGRES_USER"),
                password=os.getenv("POSTGRES_PASSWORD"),
                host="database", port="5432",
                options="-c statement_timeout=60s")
            logger.info("Connected to DB")
            process(con)
            con.commit()
            con.close()
            return
        except psycopg2.OperationalError as e:
            logger.warning("Error while connecting to DB: %s", str(e).strip())
            time.sleep(1)

    logger.critical("Failed to connect to DB")

    
if __name__ == "__main__":
    main()
