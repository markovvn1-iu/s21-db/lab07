#!/bin/bash

# task1
PGPASSWORD=$POSTGRES_PASSWORD psql -h database -U $POSTGRES_USER -d $POSTGRES_DB -a -f ex1.sql
./ex1.py

# task2
PGPASSWORD=$POSTGRES_PASSWORD psql -h database -U $POSTGRES_USER -d $POSTGRES_DB -a -f ex2.sql

echo "COMPLETE. You can shutdown it: Ctrl + C"